﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parser
{
    public class OrdenCompra
    {
        public string Provincia { get; set; }
        public string Calle { get; set; }
        public string NoApartamento { get; set; }
        public string CoPostal { get; set; }
        public string IDProducto { get; set; }
        public string NombreProducto { get; set; }
        public int Cantidad { get; set; }
        public string NombrePersona { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Cedula { get; set; }

    }
}
