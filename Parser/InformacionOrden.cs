﻿namespace Parser
{
    public class InformacionOrden
    {
        public InfoEnvio InfoEnvio { get; set; }
        public InfoProducto InfoProducto { get; set; }
        public InfoCliente InfoCliente { get; set; }
    }

    public class InfoEnvio
    {
        public string Provincia { get; set; }
        public string Calle { get; set; }
        public string NoApartamento { get; set; }
        public string CoPostal { get; set; }
    }

    public class InfoProducto
    {
        public string IDProducto { get; set; }
        public string Name { get; set; }
        public int Cantidad { get; set; }
    }

    public class InfoCliente
    {
        public string Name { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Cedula { get; set; }
    }
}
