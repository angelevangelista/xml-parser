﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Parser
{
    public static class Parser
    {
        public static string Serialize(OrdenCompra model)
        {
            string result = string.Empty;
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(InformacionOrden));
            InformacionOrden informacionOrden = new InformacionOrden()
            {
                InfoEnvio = new InfoEnvio()
                {
                    Provincia = model.Provincia,
                    Calle = model.Calle,
                    NoApartamento = model.NoApartamento,
                    CoPostal = model.CoPostal
                },
                InfoProducto = new InfoProducto()
                {
                    Cantidad = model.Cantidad,
                    IDProducto = model.IDProducto,
                    Name = model.NombreProducto
                },
                InfoCliente = new InfoCliente()
                {
                    Name = model.NombrePersona,
                    Apellido = model.Apellido,
                    Cedula = model.Cedula,
                    Telefono = model.Telefono
                }
            };

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("ns0", "http://NWMessaging.OrdenCompra");

            using (var stringWriter = new StringWriter())
            {
                using (var xmlWriter = XmlWriter.Create(stringWriter))
                {
                    xmlSerializer.Serialize(xmlWriter, informacionOrden, ns);
                    result = stringWriter.ToString();
                }
            }

            return result;
        }
    }
}
