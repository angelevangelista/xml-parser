﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Parser.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var orden = new OrdenCompra()
            {
                NombrePersona = "Black",
                Apellido = "Viper",
                Cedula = "202020202022020",
                NoApartamento = "Num",
                CoPostal = "CodigoPostal",
                Telefono = "809-000-0000",
                Calle = "Lo Fuelte",
                Provincia = "Pro",
                Cantidad = 10,
                IDProducto = "IDPRoc",
                NombreProducto = "Plasma Gun"
            };


            string result = Parser.Serialize(orden);
        }
    }
}
